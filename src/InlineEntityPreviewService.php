<?php

namespace Drupal\ief_preview;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Class EntityPreviewService.
 */
class InlineEntityPreviewService {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * The private temp store used for this preview.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $store;

  /**
   * The form state object from the node-edit form.
   *
   * @var \Drupal\Core\Form\FormStateInterface;
   */
  protected $formState;

  /**
   * Constructs a new EntityPreviewService object.
   */
  public function __construct(CurrentRouteMatch $current_route_match, PrivateTempStoreFactory $tempstore_private) {
    $this->currentRouteMatch = $current_route_match;
    $this->store = $tempstore_private;

    $node_preview_id = $this->currentRouteMatch
      ->getRawParameter('node_preview');

    if (!empty($node_preview_id)) {
      $this->formState = $this->store
        ->get('node_preview')
        ->get($node_preview_id);
    }
  }

  /**
   *
   *
   * @param \Drupal\Core\Entity\EntityInterface $original
   * @param EntityInterface $build_entity
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function replace(EntityInterface $original, $build_entity) {
    $form_state = $this->formState;

    if (empty($form_state)) {
      return $original;
    }

    $node_preview = $form_state
      ->getFormObject()
      ->getEntity();

    // Check if $original is the node_preview entity.
    $is_node_preview = FALSE;

    if (!empty($build_entity) && $build_entity->uuid() == $node_preview->uuid()) {
      $is_node_preview = TRUE;
    }

    foreach ($form_state->get('inline_entity_form') as &$widget_state) {      
      foreach ($node_preview as $field) {
        /** @var \Drupal\Core\Field\FieldDefinitionInterface $definition */
        $definition = $field->getFieldDefinition();

        // Only act on entity_reference fields.
        if ($definition->getType() != 'entity_reference') {
          continue;
        }

        // Skip empty fields.
        if (empty($widget_state['instance'])) {
          continue;
        }

        // If we are building the node, update the fields using
        // inline_entity_form.
        if ($is_node_preview && $field->getName() === $widget_state['instance']->getName()) {
          $original->{$field->getName()}->setValue($widget_state['entities']);
        }

        // Iterate through the entities saved in the ief and, if they match
        // this entity, replace the entity with the one saved in the form_state
        // object.
        foreach ($widget_state['entities'] as $delta => $entity_item) {
          // We don't need to replace those that are saved.
          if (!$entity_item['needs_save']) {
            continue;
          }

          $ief_entity = $entity_item['entity'];

          if ($ief_entity->getEntityTypeId() != $original->getEntityTypeId()) {
            continue;
          }

          if ($ief_entity->id() == $original->id()) {
            return $ief_entity;
          }
        }
      }
    }
    
    return $original;
  }

}

